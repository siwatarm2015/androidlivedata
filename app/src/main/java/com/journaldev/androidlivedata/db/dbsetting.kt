package com.journaldev.androidlivedata.db

import android.provider.BaseColumns

class dbsetting {

    companion object {
        val DB_NAME = "favourites.db"
        val DB_VERSION = 1
    }

    class DBEntry : BaseColumns {
        companion object {
            val TABLE = "fav"
            val COL_FAV_URL = "url"
            val COL_FAV_DATE = "date"
        }
    }


}