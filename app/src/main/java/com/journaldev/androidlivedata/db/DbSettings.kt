package com.journaldev.androidlivedata.db

import android.provider.BaseColumns


 class DbSettings {

    class DBEntry : BaseColumns {
        companion object {
            const val _ID = "_id"
            const val TABLE = "fav"
            const val COL_FAV_URL = "url"
            const val COL_FAV_DATE = "date"
        }
    }

    companion object {

        const val DB_NAME = "favourites.db"
        const val DB_VERSION = 1
    }
}
