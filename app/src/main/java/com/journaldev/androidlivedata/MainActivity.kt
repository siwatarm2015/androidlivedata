package com.journaldev.androidlivedata

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

import java.util.Date

class MainActivity : AppCompatActivity() {

    private var mFavAdapter: FavAdapter? = null
    private var mFavViewModel: FavouritesViewModel? = null
    private var mFav: List<Favourites>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        mFavViewModel = ViewModelProviders.of(this).get(FavouritesViewModel::class.java)
        val favsObserver = Observer<List<Favourites>> { updatedList ->
            if (mFav == null) {
                mFav = updatedList
                mFavAdapter = FavAdapter()
                recyclerView.adapter = mFavAdapter
            } else {
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

                    override fun getOldListSize(): Int {
                        return mFav!!.size
                    }

                    override fun getNewListSize(): Int {
                        return updatedList!!.size
                    }

                    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                        return mFav!![oldItemPosition].mId == updatedList!![newItemPosition].mId
                    }

                    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                        val oldFav = mFav!![oldItemPosition]
                        val newFav = updatedList!![newItemPosition]
                        return oldFav == newFav
                    }
                })
                mFavAdapter?.let { result.dispatchUpdatesTo(it) }
                mFav = updatedList
            }
        }

        fab.setOnClickListener {
            val inUrl = EditText(this@MainActivity)
            val dialog = AlertDialog.Builder(this@MainActivity)
                    .setTitle("New favourite")
                    .setMessage("Add a url link below")
                    .setView(inUrl)
                    .setPositiveButton("Add") { _, _ ->
                        val url = inUrl.text.toString()
                        val date = Date().time
                        // VM AND VIEW
                        mFavViewModel!!.addFav(url, date)
                    }
                    .setNegativeButton("Cancel", null)
                    .create()
            dialog.show()
        }

        fab2.setOnClickListener {
            val i = Intent(this,TestActivity::class.java)
            startActivity(i)
        }

        mFavViewModel!!.favs.observe(this, favsObserver)
    }

    inner class FavAdapter : RecyclerView.Adapter<FavAdapter.FavViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_row, parent, false)
            return FavViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: FavViewHolder, position: Int) {
            val favourites = mFav!![position]
            holder.mTxtUrl.text = favourites.mUrl
            holder.mTxtDate.text = Date(favourites.mDate).toString()
        }

        override fun getItemCount(): Int {
            return mFav!!.size
        }

        inner class FavViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var mTxtUrl: TextView = itemView.findViewById(R.id.tvUrl)
            var mTxtDate: TextView = itemView.findViewById(R.id.tvDate)

            init {
                val btnDelete = itemView.findViewById<ImageButton>(R.id.btnDelete)
                btnDelete.setOnClickListener {
                    val pos = adapterPosition
                    val favourites = mFav!![pos]
                    mFavViewModel!!.removeFav(favourites.mId)
                }
            }
        }
    }

}
