package com.journaldev.androidlivedata

class Favourites {

    var mId: Long = 0
    var mUrl: String
    var mDate: Long = 0

    constructor(id: Long, name: String, date: Long) {
        mId = id
        mUrl = name
        mDate = date
    }

    constructor(favourites: Favourites) {
        mId = favourites.mId
        mUrl = favourites.mUrl
        mDate = favourites.mDate
    }

}