package com.journaldev.androidlivedata

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity() {

    override fun onStart() {
        super.onStart()
        val result = calculate(10,6) { x, y ->
            x+y
        }
        testLambda{
            println("StringReturnIs : $it")
        }
        Log.d("Result", "$result")
        Log.d("onStart", "Start")
    }

    private fun testLambda(b:(a:String)-> Unit){
        b.invoke("Hello World")
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "Resume")
    }

    //lambda
    //inline not make new function object
    private inline fun calculate(x: Int,
                                 y: Int,
                                 equation: (x:Int, y:Int) -> Int) : Int {
        return equation(x,y)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        Log.d("onCreate", "Create")

        btnOK.setOnClickListener {
            var output = ""
            val i = edtInput.text.toString().replace(" ", "")
            val j = i.groupingBy { it }.eachCount()
            for (e in j.entries) {
                output += e.key + e.value.toString()
            }
            println("Output : $output")
            txtResult.text = output
        }

        btnDialog.setOnClickListener {
            val builder = AlertDialog.Builder(this).let {
                it.setTitle("Test")
                        .setCancelable(false)
                        .setMessage("Dialog Test")
                        .setPositiveButton("OK") { dialog, witch ->
                    dialog.dismiss()
                }
            }
            builder.show()
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d("onPause", "Pause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("onStop", "Stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("onDestroy", "Destroy")
    }

}
